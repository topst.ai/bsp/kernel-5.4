/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */

#include <linux/types.h>

static void putc(int c)
{
}

static inline void flush(void)
{
}

static inline void arch_decomp_setup(void)
{
}
