/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef TCC_HOTPLUG_H
#define TCC_HOTPLUG_H

int tcc_cpu_kill(unsigned int cpu);
void tcc_cpu_die(unsigned int cpu);

#endif /* TCC_HOTPLUG_H */
