// SPDX-License-Identifier: (GPL-2.0-or-later OR MIT)
/*
 * Copyright (C) Telechips Inc.
 */

#include "tcc8053.dtsi"
#include "tcc805x-linux.dtsi"

#include <dt-bindings/pmap/tcc805x/pmap-tcc805x-linux-dvrs.h>

/ {
	model = "Telechips TCC8053 Evaluation Board for DVRS";
	compatible = "telechips,tcc8053", "telechips,tcc805x";

	chosen {
		bootargs = "vmalloc=480M console=ttyAMA0,115200n8";
		stdout-path = &uart0;
	};

	i2c@16370000 {
		status = "okay";
		port-mux = <12>;
		pinctrl-names = "default";
		pinctrl-0 = <&i2c12_bus>;
	};

	vbus_supply_ehci: vbus_supply_ehci {
		status = "okay";
	};

	/* USB 2.0 Host */
	ehci_phy@11DA0010 {
		status = "okay";
	};

	ehci@11A00000 {
		status = "okay";
	};

	ohci@11A80000 {
		status = "okay";
	};
};

&reserved_memory {
	/*-----------------------------------------------------------
	* Memory for PowerVR Virtualization
	*-----------------------------------------------------------
	*/
	pmap_pvr_vz: pmap_pvr_vz {
		reg = <0x0 PVR_VZ_BASE 0x0 PMAP_SIZE_PVR_VZ>;
		no-map;
	};

	/*-----------------------------------------------------------
	 * Camera Memory
	 *-----------------------------------------------------------
	 */
	pmap_parking_gui: pmap_parking_gui {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PGL_BASE 0x0 PMAP_SIZE_CAMERA_PGL>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera_viqe: pmap_rearcamera_viqe {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_VIQE_BASE 0x0 PMAP_SIZE_CAMERA_VIQE>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera: pmap_rearcamera {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PREVIEW_BASE 0x0 PMAP_SIZE_CAMERA_PREVIEW0>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera1: pmap_rearcamera1 {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PREVIEW1_BASE 0x0 PMAP_SIZE_CAMERA_PREVIEW1>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera2: pmap_rearcamera2 {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PREVIEW2_BASE 0x0 PMAP_SIZE_CAMERA_PREVIEW2>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera3: pmap_rearcamera3 {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PREVIEW3_BASE 0x0 PMAP_SIZE_CAMERA_PREVIEW3>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera4: pmap_rearcamera4 {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PREVIEW4_BASE 0x0 PMAP_SIZE_CAMERA_PREVIEW4>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera5: pmap_rearcamera5 {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PREVIEW5_BASE 0x0 PMAP_SIZE_CAMERA_PREVIEW5>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera6: pmap_rearcamera6 {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PREVIEW6_BASE 0x0 PMAP_SIZE_CAMERA_PREVIEW6>;
		alignment = <0x1000>;
		no-map;
	};

	pmap_rearcamera7: pmap_rearcamera7 {
		compatible = "shared-dma-pool";
		reg = <0x0 CAMERA_PREVIEW7_BASE 0x0 PMAP_SIZE_CAMERA_PREVIEW7>;
		alignment = <0x1000>;
		no-map;
	};

	/*-----------------------------------------------------------
	 * Secure Area 1 (CPU X, VPU X, GPU W, VIOC R)
	 *-----------------------------------------------------------
	 */
	pmap_fb_video: fb_video {
		compatible = "pmap,fb_video";
		pmap-name = "fb_video";
		alloc-ranges = <0x0 SECURE_AREA_1_BASE 0x0 PMAP_SIZE_SECURE_AREA_1>;
		size = <0x0 PMAP_SIZE_FB_VIDEO>;
		no-map;
	};

	//pmap_fb1_video: fb1_video {
	//	compatible = "pmap,fb1_video";
	//	pmap-name = "fb1_video";
	//	alloc-ranges = <0x0 SECURE_AREA_1_BASE 0x0 PMAP_SIZE_SECURE_AREA_1>;
	//	size = <0x0 PMAP_SIZE_FB1_VIDEO>;
	//	no-map;
	//};

	//pmap_fb2_video: fb2_video {
	//	compatible = "pmap,fb2_video";
	//	pmap-name = "fb2_video";
	//	alloc-ranges = <0x0 SECURE_AREA_1_BASE 0x0 PMAP_SIZE_SECURE_AREA_1>;
	//	size = <0x0 PMAP_SIZE_FB2_VIDEO>;
	//	no-map;
	//};

	//pmap_fb3_video: fb3_video {
	//	compatible = "pmap,fb3_video";
	//	pmap-name = "fb3_video";
	//	alloc-ranges = <0x0 SECURE_AREA_1_BASE 0x0 PMAP_SIZE_SECURE_AREA_1>;
	//	size = <0x0 PMAP_SIZE_FB3_VIDEO>;
	//	no-map;
	//};

	/*-----------------------------------------------------------
	 * Secure Area 2 (CPU X, VPU X, GPU X, VIOC R/W)
	 *-----------------------------------------------------------
	 */
	pmap_overlay: overlay {
		compatible = "pmap,overlay";
		pmap-name = "overlay";
		alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
		size = <0x0 PMAP_SIZE_OVERLAY>;
		no-map;
	};

	pmap_overlay1: overlay1 {
		compatible = "pmap,overlay1";
		pmap-name = "overlay1";
		alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
		size = <0x0 PMAP_SIZE_OVERLAY1>;
		no-map;
	};

	pmap_osd: osd {
		compatible = "pmap,osd";
		pmap-name = "osd";
		alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
		size = <0x0 PMAP_SIZE_OSD>;
		no-map;
	};

	pmap_overlay_rot: overlay_rot {
		compatible = "pmap,overlay_rot";
		pmap-name = "overlay_rot";
		alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
		size = <0x0 PMAP_SIZE_OVERLAY_ROT>;
		no-map;
	};

	pmap_viqe0: viqe0 {
		compatible = "pmap,viqe";
		pmap-name = "viqe";
		alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
		size = <0x0 PMAP_SIZE_VIQE0>;
		no-map;
	};

	//pmap_viqe1: viqe1 {
	//	compatible = "pmap,viqe1";
	//	pmap-name = "viqe1";
	//	alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
	//	size = <0x0 PMAP_SIZE_VIQE1>;
	//	no-map;
	//};

	pmap_v4l2_vout0: v4l2_vout0 {
		compatible = "pmap,v4l2_vout0";
		pmap-name = "v4l2_vout0";
		alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
		size = <0x0 PMAP_SIZE_V4L2_VOUT0>;
		no-map;
	};

	pmap_v4l2_vout1: v4l2_vout1 {
		compatible = "pmap,v4l2_vout1";
		pmap-name = "v4l2_vout1";
		alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
		size = <0x0 PMAP_SIZE_V4L2_VOUT1>;
		no-map;
	};

	pmap_fb_wmixer: fb_wmixer {
		compatible = "pmap,fb_wmixer";
		pmap-name = "fb_wmixer";
		alloc-ranges = <0x0 SECURE_AREA_2_BASE 0x0 PMAP_SIZE_SECURE_AREA_2>;
		size = <0x0 PMAP_SIZE_FB_WMIXER>; //
		no-map;
	};

	/*-----------------------------------------------------------
	 * Secure Area 3 (CPU X, VPU R/W, GPU X, VIOC R)
	 *-----------------------------------------------------------
	 */
	pmap_video: video {
		compatible = "telechips,pmap";
		telechips,pmap-name = "video";
		alloc-ranges = <0x0 SECURE_AREA_3_BASE 0x0 PMAP_SIZE_SECURE_AREA_3>;
		size = <0x0 PMAP_SIZE_VIDEO>;
		telechips,pmap-secured = <3>;
		no-map;
	};

	pmap_video_ext: video_ext {
		compatible = "telechips,pmap";
		telechips,pmap-name = "video_ext";
		alloc-ranges = <0x0 SECURE_AREA_3_BASE 0x0 PMAP_SIZE_SECURE_AREA_3>;
		size = <0x0 PMAP_SIZE_VIDEO_EXT>;
		telechips,pmap-secured = <3>;
		no-map;
	};

	pmap_video_ext2: video_ext2 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "video_ext2";
		alloc-ranges = <0x0 SECURE_AREA_3_BASE 0x0 PMAP_SIZE_SECURE_AREA_3>;
		size = <0x0 PMAP_SIZE_VIDEO_EXT2>;
		telechips,pmap-secured = <3>;
		no-map;
	};

	pmap_video_sw: video_sw {
		compatible = "telechips,pmap";
		telechips,pmap-name = "video_sw";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_VIDEO_SW_EXTRA>;
		no-map;
	};

	/*-----------------------------------------------------------
	 * Shared Memory with pmap_video :: only for enc_main pmap.
	 *-----------------------------------------------------------
	 */
	pmap_enc_main: enc_main {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_main";
		alloc-ranges = <0x0 SECURE_AREA_3_BASE 0x0 PMAP_SIZE_SECURE_AREA_3>;
		pmap-shared = <&pmap_video>;
		pmap-shared-size = <0x0 PMAP_SIZE_ENC>;
		pmap-offset = <0x0 VIDEO_MAIN_SIZE>;
		size = <0x0 0x0>;
		no-map;
	};

	pmap_enc_ext: enc_ext {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT>;
		no-map;
	};

	pmap_enc_ext2: enc_ext2 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext2";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT2>;
		no-map;
	};

	pmap_enc_ext3: enc_ext3 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext3";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT3>;
		no-map;
	};

	pmap_enc_ext4: enc_ext4 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext4";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT4>;
		no-map;
	};

	pmap_enc_ext5: enc_ext5 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext5";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT5>;
		no-map;
	};

	pmap_enc_ext6: enc_ext6 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext6";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT6>;
		no-map;
	};

	pmap_enc_ext7: enc_ext7 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext7";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT7>;
		no-map;
	};

	pmap_enc_ext8: enc_ext8 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext8";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT8>;
		no-map;
	};

	pmap_enc_ext9: enc_ext9 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext9";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT9>;
		no-map;
	};

	pmap_enc_ext10: enc_ext10 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext10";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT10>;
		no-map;
	};

	pmap_enc_ext11: enc_ext11 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext11";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT11>;
		no-map;
	};

	pmap_enc_ext12: enc_ext12 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext12";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT12>;
		no-map;
	};

	pmap_enc_ext13: enc_ext13 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext13";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT13>;
		no-map;
	};

	pmap_enc_ext14: enc_ext14 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext14";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT14>;
		no-map;
	};

	pmap_enc_ext15: enc_ext15 {
		compatible = "telechips,pmap";
		telechips,pmap-name = "enc_ext15";
		alloc-ranges = <0x0 RESERVED_HEAP_BASE 0x0 RESERVED_HEAP_SIZE>;
		size = <0x0 PMAP_SIZE_ENC_EXT15>;
		no-map;
	};
};

&uart0 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart18_data>;
	status = "okay";
};

/* bluetooth */
&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart20_data &uart20_rtscts>;
	dmas = <&udma1 2 2 &udma1 3 2>;
	dma-names = "tx", "rx";
	auto-poll;
	status = "okay";
};

&ictc {
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&ictc_f_in>;
        f-in-gpio = <&gph 5 0>;
        //f-in-rtc-wkup;
        r-edge = <100>;
        f-edge = <100>;
        edge-matching-value = <100>;
        time-out = <0>;
        duty-rounding-value = <0x0ffffff0>;
        prd-rounding-value = <0x0fffffff0>;
        flt-f-mode = <0x3>;
        flt-r-mode = <0x3>;
        cmp-err-sel = <0>;
        abs-sel = <1>;
        edge-sel = <0>;
        tck-pol = <0>;
        tck-sel = <0>;
        lock-en = <0>;
        //r-edge-int
        //f-edge-int;
        //df-cnt-full-int
        f-chg-int;
        //d-chg-int;
        //e-cnt-full-int;
        //to-cnt-full-int;
        //nf-ed-cnt-full-int;
        //time-stamp-cnt;
};

&gmac {
	status = "okay";
	compatible = "snps,dwmac-4.10a","telechips,gmac";
	phyrst-gpio = <&gpmc 11 0>;
	phy-interface = "rgmii";
	interrupt-names = "macirq";
	phy-mode = "rgmii";
	snps,txpbl = <8>;
	snps,rxpbl = <2>;
	//ecid-mac-addr;
	txclk-o-dly = <31>;
	txclk-o-inv = <0>;
	txclk-i-dly = <0>;
	txclk-i-inv = <0>;
	txen-dly = <0>;
	txer-dly = <0>;
	txd0-dly = <0>;
	txd1-dly = <0>;
	txd2-dly = <0>;
	txd3-dly = <0>;
	txd4-dly = <0>;
	txd5-dly = <0>;
	txd6-dly = <0>;
	txd7-dly = <0>;
	rxclk-i-dly = <0>;
	rxclk-i-inv = <0>;
	rxdv-dly = <0>;
	rxer-dly = <0>;
	rxd0-dly = <0>;
	rxd1-dly = <0>;
	rxd2-dly = <0>;
	rxd3-dly = <0>;
	rxd4-dly = <0>;
	rxd5-dly = <0>;
	rxd6-dly = <0>;
	rxd7-dly = <0>;
	crs-dly = <0>;
	col-dly = <0>;
	pinctrl-names = "default", "mii", "rmii", "gmii", "rgmii";
	pinctrl-0 = <>;
	pinctrl-1 = <&gmac1_mdc &gmac1_mdio &gmac1_col &gmac1_crs &gmac1_txer &gmac1_rxer
		&gmac1_txclk &gmac1_txen &gmac1_txd0 &gmac1_txd1 &gmac1_txd2 &gmac1_txd3
		&gmac1_rxclk &gmac1_rxdv &gmac1_rxd0 &gmac1_rxd1 &gmac1_rxd2 &gmac1_rxd3>;
	pinctrl-2 = <&gmac1_mdc &gmac1_mdio
		&gmac1_txclk &gmac1_txen &gmac1_txd0 &gmac1_txd1
		&gmac1_rxer &gmac1_rxdv &gmac1_rxd0 &gmac1_rxd1>;
	pinctrl-3 = <&gmac1_mdc &gmac1_mdio &gmac1_col &gmac1_crs &gmac1_txer &gmac1_rxer
		&gmac1_txclk &gmac1_txen &gmac1_txd0 &gmac1_txd1 &gmac1_txd2 &gmac1_txd3
		&gmac1_txd4 &gmac1_txd5 &gmac1_txd6 &gmac1_txd7
		&gmac1_rxclk &gmac1_rxdv &gmac1_rxd0 &gmac1_rxd1 &gmac1_rxd2 &gmac1_rxd3
		&gmac1_rxd4 &gmac1_rxd5 &gmac1_rxd6 &gmac1_rxd7>;
	pinctrl-4 = <&gmac1_mdc &gmac1_mdio
		&gmac1_txclk &gmac1_txen &gmac1_txd0 &gmac1_txd1 &gmac1_txd2 &gmac1_txd3
		&gmac1_rxclk &gmac1_rxdv &gmac1_rxd0 &gmac1_rxd1 &gmac1_rxd2 &gmac1_rxd3>;
#if 0 /* If using fixed link, enable this */
	fixed-link {
	speed = <1000>;
	full-duplex;
};
#endif
};

&gpsb0 {
	status = "okay";
	gpsb-port = <22>;
	pinctrl-names = "idle", "active";
	pinctrl-0 = <&gpsb22_bus_idle>;
	pinctrl-1 = <&gpsb22_bus_spi>;

	/* cs-gpios */
	cs-gpios = <&gpg 8 0>,<&gpa 30 0>;

	#address-cells = <1>;
	#size-cells = <0>;
	spidev@0 {
		compatible = "rohm,dh2228fv";
		reg = <0>;
		spi-max-frequency = <20000000>;
	};
	spidev@1 {
		compatible = "rohm,dh2228fv";
		reg = <1>;
		spi-max-frequency = <20000000>;
	};
};

&gpsb1 {
	status = "okay";
	gpsb-port = <15>;
	pinctrl-names = "idle", "active";
	pinctrl-0 = <&gpsb15_bus_idle>;
	pinctrl-1 = <&gpsb15_bus_tsif>;
};

&gpsb2 {
	status = "okay";
	gpsb-port = <16>;
	pinctrl-names = "idle", "active";
	pinctrl-0 = <&gpsb16_bus_idle>;
	pinctrl-1 = <&gpsb16_bus_tsif>;
};
