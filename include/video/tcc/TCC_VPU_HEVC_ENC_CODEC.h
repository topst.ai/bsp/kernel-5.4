// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) Telechips Inc.
 */
/*
 *   include/video/tcc/TCC_VPU_HEVC_ENC_CODEC.h
 *   Author:  <linux@telechips.com>
 *   Created: Apr 29, 2020
 *   Description: header to interface TCC HEVC ENC Lib.
 */

#include "TCC_VPU_HEVC_ENC.h"

#ifndef _TCC_VPU_HEVC_ENC_CODEC_H_
#define _TCC_VPU_HEVC_ENC_CODEC_H_

#ifndef RETCODE_MULTI_CODEC_EXIT_TIMEOUT
#define RETCODE_MULTI_CODEC_EXIT_TIMEOUT    99
#endif

#ifndef SZ_1M
#define SZ_1M   (1024 * 1024)
#endif

#endif // _TCC_VPU_HEVC_ENC_CODEC_H_
