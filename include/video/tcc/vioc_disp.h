/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */
#ifdef CONFIG_ARCH_TCC803X
#include "tcc803x/vioc_disp.h"
#endif

#ifdef CONFIG_ARCH_TCC805X
#include "tcc805x/vioc_disp.h"
#endif

#ifdef CONFIG_ARCH_TCC897X
#include "tcc897x/vioc_disp.h"
#endif
