/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */
#ifdef CONFIG_ARCH_TCC803X
#include "tcc803x/vioc_mc.h"
#endif

#ifdef CONFIG_ARCH_TCC805X
#include "tcc805x/vioc_mc.h"
#endif
