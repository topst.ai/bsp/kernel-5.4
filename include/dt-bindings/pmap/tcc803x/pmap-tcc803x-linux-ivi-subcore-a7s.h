/* SPDX-License-Identifier: (GPL-2.0+ OR MIT) */
/*
 * Copyright (C) 2020 Telechips Inc.
 */
#ifndef DT_BINDINGS_PMAP_TCC803X_LINUX_IVI_SUBCORE_A7S_H
#define DT_BINDINGS_PMAP_TCC803X_LINUX_IVI_SUBCORE_A7S_H

#include <dt-bindings/pmap/tcc803x/pmap-tcc803x-linux-ivi-subcore-a7s-display.h>

#define RESERVED_MEM_BASE		(0x90000000)
#define RESERVED_MEM_SIZE		(0x10000000)

#define	FB_VIDEO_BASE			(RESERVED_MEM_BASE)

#define	CAMERA_PGL_BASE			(FB_VIDEO_BASE + PMAP_SIZE_FB_VIDEO)
#define	CAMERA_VIQE_BASE		(CAMERA_PGL_BASE + PMAP_SIZE_CAMERA_PGL)
#define	CAMERA_PREVIEW_BASE		(CAMERA_VIQE_BASE + PMAP_SIZE_CAMERA_VIQE)
#define	CAMERA_PREVIEW1_BASE		(CAMERA_PREVIEW_BASE + PMAP_SIZE_CAMERA_PREVIEW0)
#define	CAMERA_PREVIEW2_BASE		(CAMERA_PREVIEW1_BASE + PMAP_SIZE_CAMERA_PREVIEW1)
#define	CAMERA_PREVIEW3_BASE		(CAMERA_PREVIEW2_BASE + PMAP_SIZE_CAMERA_PREVIEW2)
#define	CAMERA_PREVIEW4_BASE		(CAMERA_PREVIEW3_BASE + PMAP_SIZE_CAMERA_PREVIEW3)
#define	CAMERA_PREVIEW5_BASE		(CAMERA_PREVIEW4_BASE + PMAP_SIZE_CAMERA_PREVIEW4)
#define	CAMERA_PREVIEW6_BASE		(CAMERA_PREVIEW5_BASE + PMAP_SIZE_CAMERA_PREVIEW5)
#define	CAMERA_LASTFRAME_BASE		(CAMERA_PREVIEW6_BASE + PMAP_SIZE_CAMERA_PREVIEW6)

#define	RESERVED_HEAP_BASE		(CAMERA_LASTFRAME_BASE + PMAP_SIZE_CAMERA_LASTFRAME)
#define	RESERVED_HEAP_SIZE		(RESERVED_MEM_BASE + RESERVED_MEM_SIZE \
						- RESERVED_HEAP_BASE)

#endif//DT_BINDINGS_PMAP_TCC803X_LINUX_IVI_SUBCORE_A7S_H
