// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (C) 2020 Telechips Inc.
 */
#ifndef DT_BINDINGS_PMAP_TCC803X_ANDROID_IVI_H
#define DT_BINDINGS_PMAP_TCC803X_ANDROID_IVI_H

#define PMAP_BASE 0x40000000
#include <dt-bindings/pmap/tcc803x/pmap-tcc803x-graphic.h>
#include <dt-bindings/pmap/tcc803x/pmap-tcc803x-android-ivi-display.h>
#include <dt-bindings/pmap/tcc803x/pmap-tcc803x-video.h>

#endif//DT_BINDINGS_PMAP_TCC803X_ANDROID_IVI_H
