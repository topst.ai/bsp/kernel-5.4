// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (C) 2020 Telechips Inc.
 */
#ifndef DT_BINDINGS_PMAP_TCC805X_ANDROID_IVI_H
#define DT_BINDINGS_PMAP_TCC805X_ANDROID_IVI_H

#define PMAP_BASE 0x80000000
#include <dt-bindings/pmap/tcc805x/pmap-tcc805x-graphic.h>
#include <dt-bindings/pmap/tcc805x/pmap-tcc805x-android-ivi-display.h>
#include <dt-bindings/pmap/tcc805x/pmap-tcc805x-android-ivi-video.h>

#endif//DT_BINDINGS_PMAP_TCC805X_ANDROID_IVI_H
