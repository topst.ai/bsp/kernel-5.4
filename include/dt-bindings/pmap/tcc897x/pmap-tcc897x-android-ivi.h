// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (C) 2020 Telechips Inc.
 */
#ifndef DT_BINDINGS_PMAP_TCC897X_ANDROID_IVI_H
#define DT_BINDINGS_PMAP_TCC897X_ANDROID_IVI_H

#define PMAP_BASE 0x87800000
#include <dt-bindings/pmap/tcc897x/pmap-tcc897x-graphic.h>
#include <dt-bindings/pmap/tcc897x/pmap-tcc897x-android-ivi-display.h>
#include <dt-bindings/pmap/tcc897x/pmap-tcc897x-video.h>

#endif//DT_BINDINGS_PMAP_TCC897X_ANDROID_IVI_H
