// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (C) 2020 Telechips Inc.
 */
#ifndef DT_BINDINGS_PMAP_TCC897X_LINUX_CLUSTER_H
#define DT_BINDINGS_PMAP_TCC897X_LINUX_CLUSTER_H

#define PMAP_BASE 0x87800000
#include <dt-bindings/pmap/tcc897x/pmap-tcc897x-linux-cluster-display.h>

#define FB_VIDEO_BASE                   (PMAP_BASE)

#endif//DT_BINDINGS_PMAP_TCC897X_LINUX_CLUSTER_H
