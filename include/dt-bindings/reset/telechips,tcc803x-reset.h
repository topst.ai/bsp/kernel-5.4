/* SPDX-License-Identifier: (GPL-2.0+ OR MIT) */
/*
 * Copyright (C) 2019 Telechips Inc.
 */

#ifndef __DT_BINDINGS_TELECHIPS_TCC803X_RESET_H
#define __DT_BINDINGS_TELECHIPS_TCC803X_RESET_H

#define RESET_CPUMP		0
#define RESET_CPUAP		1
#define RESET_CPUBUS		2
#define RESET_CMBUS		3
#define RESET_MEMBUS		4
#define RESET_VBUS		5
#define RESET_HSIOBUS		6
#define RESET_SMUBUS		7
#define RESET_3D		8
#define RESET_DDIBUS		9
#define RESET_2D		10
#define RESET_IOBUS		11
#define RESET_VCORE		12
#define RESET_CHEVC		13
#define RESET_VHEVC		14
#define RESET_BHEVC		15
#define RESET_PCIE		16
#define RESET_U30		17
#define RESET_SDMA		18

#endif /* __DT_BINDINGS_TELECHIPS_TCC803X_RESET_H */
