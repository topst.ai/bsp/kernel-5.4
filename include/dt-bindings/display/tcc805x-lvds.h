/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) 2020 Telechips Inc.
 */
#ifndef __DT_BINDINGS_TCC805X_LVDS_H
#define __DT_BINDINGS_TCC805X_LVDS_H

#define PHY_PORT_S0 0
#define PHY_PORT_S1 1
#define PHY_PORT_D0 2
#define PHY_PORT_D1 3

#define CLK_LANE 0
#define DATA0_LANE 1
#define DATA1_LANE 2
#define DATA2_LANE 3
#define DATA3_LANE 4

#define LVDS_DUAL 0
#define	LVDS_SINGLE 1

#endif
