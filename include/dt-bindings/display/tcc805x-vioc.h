/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) 2020 Telechips Inc.
 */
#ifndef __DT_BINDINGS_TCC805X_VIOC_H
#define __DT_BINDINGS_TCC805X_VIOC_H

#include <video/tcc/tcc805x/vioc_global.h>
#endif /* __DT_BINDINGS_TCC805X_VIOC_H */
