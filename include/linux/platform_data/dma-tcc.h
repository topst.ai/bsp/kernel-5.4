/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef DMA_TCC_H
#define DMA_TCC_H

struct tcc_dma_platform_data {
	s32 nr_channels;
};

#endif
