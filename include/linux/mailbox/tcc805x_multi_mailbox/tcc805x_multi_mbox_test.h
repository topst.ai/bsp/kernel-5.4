/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef TCC805X_MULTI_MBOX_TEST_H

#define TCC805X_MULTI_MBOX_TEST_H

#define MBOX_TEST_MAGIC ('M')

#define IOCTL_MBOX_TEST	(_IO(MBOX_TEST_MAGIC, 1))


#endif
