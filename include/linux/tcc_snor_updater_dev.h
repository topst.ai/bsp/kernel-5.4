// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) Telechips Inc.
 */


#ifndef TCC_SNOR_UPDATER_DEV_H
#define TCC_SNOR_UPDATER_DEV_H

 #include <uapi/linux/tcc_snor_updater_dev.h>

#endif
