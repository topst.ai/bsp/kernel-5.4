/* SPDX-License-Identifier: GPL-2.0-or-later */

#ifndef TCCVIN_DIAGNOSTICS_H
#define TCCVIN_DIAGNOSTICS_H

#include <media/v4l2-common.h>

extern int tccvin_diag_cif_port(struct v4l2_subdev *sd);

#endif//TCCVIN_DIAGNOSTICS_H
