/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef TCC_MIPI_CSI2_H
#define TCC_MIPI_CSI2_H

#ifndef ON
#define ON		1
#endif

#ifndef OFF
#define OFF		0
#endif

#define MIPI_CSI2_0	0
#define MIPI_CSI2_1	1

#define TCC_MIPI_CSI2_DRIVER_NAME "tcc-mipi-csi2"
#define TCC_MIPI_CSI2_SUBDEV_NAME TCC_MIPI_CSI2_DRIVER_NAME

#endif
