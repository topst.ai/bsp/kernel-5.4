/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) Telechips Inc.
 */

#ifndef TCC_SNOR_UPDATER_CRC8_H
#define TCC_SNOR_UPDATER_CRC8_H

uint32_t tcc_snor_calc_crc8(const u8 *base, uint32_t length);

#endif
