/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2008-2019 Telechips Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see the file COPYING, or write
 * to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

extern unsigned char gG2D_Dithering_en;


//static ssize_t tccxxx_grp_write(
//	struct file *filp, const char __user *buffer,
//	size_t count, loff_t *ppos);
//static ssize_t tccxxx_grp_read(struct file *file,
//	const char __user *buffer, size_t count, loff_t *ppos);
//static void tccxxx_grp_vm_open(struct vm_area_struct *vma);
//static void tccxxx_grp_vm_close(struct vm_area_struct *vma);
//static int tccxxx_grp_mmap(struct file *file, struct vm_area_struct *vma);
//static int tccxxx_grp_ioctl(struct inode *inode,
//	struct file *file, unsigned int cmd, void *arg);
//static int tccxxx_grp_release(struct inode *inode, struct file *file);
//static int tccxxx_grp_open(struct inode *inode, struct file *file);
//static irqreturn_t tccxxx_grp_handler(int irq,
//	void *dev_id, struct pt_regs *reg);


